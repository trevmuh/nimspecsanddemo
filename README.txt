VIDEO DEMONSTRATION IN THE COMMITS SECTION:

Link to the Video Demo: https://youtu.be/AG-pST-lq1Y

GENERAL INFORMATION:

I designed and coded this game as part of an individual project during a course
I took on Object Oriented Programming the summer of 2018.  The video contains 
a demonstration of a NIM game in action.  The video does NOT attempt to teach 
methods and strategies of playing Nim.  It only shows how to play the game as 
designed and how to use its features.  The full game with documentation, 
header files, and implementation files is approximately 2,000 lines of code.

TECHNICAL SPECIFICATIONS:

You can see the technical programming C++ specification files and a README 
file at the following BitBucket repository if you are interested in coding 
the game yourself.  The game takes advantage of object oriented design, 
inheritance, polymorphism, advanced pointers, vectors of pointers, memory 
allocation and deallocation, data structures such as vectors and arrays, 
exception handling, command line coding, and more.  For the technical specs, 
go to https://bitbucket.org/trevmuh/nimspecsanddemo/ .  No implementation 
files (source code) are at this link.  The repository only 
contains the game specification files, a link to this video, and a README file.

The game contains save and load features where a game can be saved during 
play.  Also, a game can be resumed with the load feature.  Another feature 
prevents users from entering inappropriate information 
(getUserInteger function).  For example, if an integer from 1 to 25 is 
required, a user's response of "99" or "abcd" won't crash the game.  The 
program simply asks for an appropriate integer within a specific range.

The save() load() and getUserInteger() functions are located in the game.h 
header file.


The NIM Game:

Nim is a two player game that begins with one or more piles of stones.  Each 
player chooses one pile and removes stones (a minimum of 1 and a maximum of 
the size of the pile).  Nim has different versions.  However, the one this 
game uses is the last player to remove a stone loses the game.  

Starting the Game:

This implementation of NIM begins with a client file called nim.cpp (not 
included in this repository).  The following is a description of how nim.cpp 
works.  The user begins the game one of two ways: 1.) Enter space-separated 
integers.  The number of piles of stones (from 1 to 8) followed by the number 
of stones in each pile, or 2.) Enter a file including its path.

The following is a C++ cerr message the program outputs if it receives improper 
command-line arguments.  The message should provide a clear understanding of 
how the game begins.

*************************Source code begins ***************************** 

" cerr << "\nWARNING--CRITICAL ERROR: The game has been stopped "
             << "\nbecause there's a problem with the information supplied."
             << "\nRead the usage information below to determine how to "
             << "\nproperly start the game. "
             << "\n\nStarting the NIM Game:"
             << "\n\nNIM has two ways to begin: Load a saved game or begin a"
             << "\nnew game.";
        cerr << "\nA.  STARTING A NEW GAME."
             << "\nEnter the file's path after \"./nim \" to the saved game at"
             << "\nthe command line."
             << "\nThe file's path can contain no spaces, and the file path "
             << "\nmust be the only command line argument.";
        cerr << "\nExample: ./nim C:\\\\user\\\\myGames\\\\nim.data without the "
             << "\nquotes.  Notice the double backslash.  You must use a "
             << "\ndouble backslash instead of a single backslash to enter"
             << "\nsubdirectories.";
        cerr << "\nAll numbers must be integers.  The first number is n and "
             << "\nrepresents the number of piles of stones.  This number n "
             << "\nmust be from " << minPilesAllowed << " to "
             << maxPilesAllowed << ", inclusive.";
        cerr << "\n\nThe next n space-separated integers represent the number"
             << "\nof stones in the n piles.  Each pile must have from \n"
             << minStonesAllowed << " to "<< maxStonesAllowed << " stones in "
             << "it.  Thus, " << ( maxStonesAllowed + 8 ) << " is an invalid "
             << "\nnumber of stones for a pile. ";
        cerr << "\n\nNote: Your n must match the number of piles supplied. For"
             << "\nexample, if n = 5, then your input should look something like"
             << "\n5  2 10 6 4 7, where n = 5, the number of piles of stones,"
             << "\nand 2 10 6 4 7 are the five piles containing 2, 10, 6, 4,"
             << "\nand 7 stones, respectively.\n\n";" 

*************************Source code ends ***************************** 


Once the user enters a valid saved game state or enters piles of stones, the 
method Game::play() is called ( the play() function in the class Game ).  

			 
The NIM History:

NIM is an ancient game whose origins are said to be in China.  The game was 
later brought to Europe in the 1400's.  A reference to the game, its playing 
strategies, and its origin are located at https://en.wikipedia.org/wiki/Nim.


Special Notes:

Because of screen output formatting, I limited the number of piles of stones to 
eight.  More piles than eight caused the tab-separated output to wrap to the 
next line.  I also limited the number of stones in a pile to 1000.  Ideally, 
numbers close to the maximum value of an integer could work, but caution 
should be used to avoid integer overflow. 

