/**smartAIPplayer.h is the class specification for class SmartAIPlayer. This
 * class is derived from AIPlayer. The primary role of this class is to
 * specify the bestMove method, a method that always seeks to make the best
 * move possible during its turn.
 */

#ifndef _SMARTAIPLAYER_H_
#define _SMARTAIPLAYER_H_

#include "aiPlayer.h"
#include <vector>



class SmartAIPlayer: public AIPlayer
{

    private:

                 /**Takes a pile of stones as the 1st parameter and counts the 
				 * number of piles with exactly one stone.  This value is 
				 * returned as the second reference parameter.  Finds the index 
				 * of the last pile that has multiple stones and returns the 
				 * value as the second reference parameter.  If all piles have 
				 one stone, then -1 is returned.
				 */
                 void countPilesWithOneStone( const vector<int>&, int&, int& );



                /**
                 * This method processes the case of number of piles > 2 and
                 * all piles have one stone except one pile.  The first
                 * parameter is the piles vector.  The second parameter is the
                 * number of piles with exactly one stone.  The third parameter
                 * is the index of the one pile with multiple stones.
                 */
                void checkPilesAllOnesExceptOne( vector< int >&, int, int  );


                /**
                 * Check piles when exactly two piles remain.  Make the best
                 * move accordingly.  The piles of stones are sent into the 
				 * method as a parameter.
                 */
                void checkExactlyTwoPiles( vector< int >& );




                /**
                 * Check piles when exactly one pile remains.  Make the best
                 * move accordingly.  The piles of stones are sent into the 
				 * method as a parameter.
                 */
                void checkExactlyOnePile( vector< int >& );



                /**
                 * The computeNimSum method calculates the nim sum of the
                 * piles of stones and return the nimSum.  The piles of stones 
                 * are sent into the method as a parameter.
                 */
                int computeNimSum( const vector< int > &piles ) const;



               /**The bestMove() method overrides AIPlayer:: move().
                * Method bestMove() always chooses the best move in an
                * attempt to win the game.  Once the SmartAIPlayer is in
                * a winning position, it cannot lose the game because it
                * always makes perfect moves until it wins.
                */
               void bestMove( vector<int>& );



    public:
              /**
                * Default constructor sets the pointer's name and playerType to 
				* "SmartAIPlayer", and the variables isYourTurn and
                * isTheWinner to false.
                *
                */
               SmartAIPlayer();


               /* Default destructor */
               ~SmartAIPlayer();

               /**
                * Overrides AIPlayer::move() with its own method called
                * bestMove().  Takes a vector of piles of stones as a parameter.
                */
               void move( vector<int>& );



};

#endif
