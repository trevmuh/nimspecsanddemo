/**player.h is an abstract base class for derived classes HumanPlayer and
 * AIPlayer.  This class defines a player that can take actions in the game.
 * Player has a pure virtual function moves() to be implemented by each of
 * the different types of players.  Because it is abstract, the class cannot 
 * instantiate a Player object.
 */

#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <vector>
#include <string>

using namespace std;


class Player
{

     private:

                 /**A variable to hold the name of the player pointer.  For
                  * example, a name could be Player1 or Computer.  Also, users
                  * can supply a name in some game modes.
                  *
                  */
                 string name;


                 /**A description of the type of player--HumanPlayer or
                  * AIPlayer.
                  */
                 string playerType;


                 /**Holds the status of whether or not it's the player's turn. */
                 bool isYourTurn;


                 /**Holds the status of whether the user has won the game. */
                 bool isTheWinner;


     public:
                /**
                 * pure virtual function implemented by derived classes; the
                 * function describes how the players move.  The function takes 
				 * a vector of piles of stones as a parameter
                 */
                virtual void move( vector<int>& ) = 0;


                /**virtual destructor for class Player */
                virtual ~Player() {}


                /**Returns the object's name. */
                string getName() const;


                /**Returns the state of the variable isYourTurn. */
                bool getIsYourTurn() const;


                /**Returns the state of the variable isTheWinner. */
                bool getIsTheWinner() const;


                /**Returns name of the playerType */
                string getPlayerType() const;


                /**Sets the name of the player using a string parameter. */
                void setName( string );

                /**Sets the name of the type of player. */
                void setPlayerType( string );


                /**Sets the name of the isTheWinner using a bool a parameter.*/
                void setIsTheWinner( bool );


                /**Sets the value of isYourTurn */
                void setIsYourTurn( bool );

};

#endif


