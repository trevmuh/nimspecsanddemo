/**
 * game.h is a class that's responsible for representing the game state (i.e.
 * the piles, the status of the piles, the current turn, etc.).  Players are
 * able to play a game with any combination of 3 players, Human, AI, or SmartAI.
 * Examples: HumanPlayer vs. HumanPlayer, HumanPlayer vs. SmartAIPlayer, or 
 * SmartAIPlayer vs. SmartAIPlayer.  This class also allows players to save 
 * the game between plays.
 *
 */
 
 
#ifndef _GAME_H_
#define _GAME_H_

#include "player.h"
#include <vector>



class Game
{
    private:
                /** The total number of piles of stones. */
                int numberOfPiles;

                /**
                 * The integer representing the type of game mode.  The user
                 * selects one of six modes after selecting stones and piles
                 * from the command line.
                 */
                int playerMode;

                /**
                 * In HumanPlayer vs. AIPlayer or SmartAIPlayer, the user can
                 * select who goes first or allow the program to randomly
                 * select the first player.
                 */
                int firstPlayerChoice;


                /** 
				 * A bool value of whether a file was loaded to resume
                 * a saved game.
                 */
                bool wasGameResumed;


                /**
                 * When a saved game is loaded, this value confirms whether
                 * it's playerA's turn.
                 */
                bool isFirstPlayerA;


                /**
				 * The text file path supplied by the user either to save
                 * a game or to load and resume a game.
                 */
                string filePath;


                /**
                 * A vector of integers representing piles of stones of various
                 * sizes.  The user will determine the size of each pile and the
                 * number of piles at the beginning of the game from the
                 * command line.
                 */
                vector< int > piles;



                /** 
				 * A vector of pointers that holds the game's players and 
                 * relevant information about the player
				 */
                vector< Player* > players;



                /**
                 * The step function is responsible for alternating 
				 * the players' turns and forcing players to make moves.  Also,
				 * the step function causes the player board to update after 
				 * each player's move.
                 */
                void step( );




                /** Prints the current game state to the standard output. */
                void display() const;



                /** 
				 * Saves the current game state.  Items saved are the number 
                 * of piles, the exact number of stones in each pile, the types 
				 * of players (HumanPlayer, SmartAIPlayer, etc.), and which 
				 * player is to make the first move.  Other items such as 
				 * human players' names can easily be configured into the 
				 * save method.  The load and save methods are dependent on 
				 * one another.  Changes in the input or output of one will 
				 * typically necessitate a change for the other method. 				 
				 */
                void save();



                /**
                 * Optionally loads a previously saved game from the command
                 * line if a user chooses the option to load a game.  The load 
				 * and save methods are dependent on one another.  Changes in 
                 * the input or output of one will typically necessitate a 
				 * change for the other method. 				 
                 */
                void load();


                /**
                 * Prompts user to choose the types of players( AIPlayer,
                 * HumanPlayer, SmartAIPlayer, etc.) and fills the Game class's
                 * players vector with the players.  Also, sets the playerMode
                 * when the game is not a resumed game.  If the game is loaded
                 * from a saved game, then only the player vectors are filled.
                 */
                 void choosePlayers();

				 

                 /**
                  * Sets the human players' personal names after the players 
				  * are chosen.  For example, if a human want to be called 
				  * "TopDev," then this method will set the name to the same.  
				  * The computer gets generic names like "AIPlayer2" or 
				  * "SmartAIPlayer1."  
				  * This method takes an integer representing the playerMode.
				  * The playerMode helps the method determine what types of 
				  * names to expect. 
                  */
                 void setPlayersNames( int );

				 
                 /**
                  * Displays the winner after the game ends.
                  */
                 void announceWinner();


    public:

                /** default constructor */
                Game();


                /**
                 * The constructor responsible for filling the integer
                 * vector with piles of stones.  The first param is
                 * the number of command-line arguments, while the
                 * second argument contains the command-line character
                 * array of pointers.
                 */
                Game( int, char *argv[] );



                /**
                 * Class destructor needed to destroy the vector of Player
                 * pointers.
                 */
                ~Game();


                /**The most critical game method that holds the step() method 
				 * which causes the players to take turns and prints an 
				 * updated game state after each turn.  This section is where 
				 * most of the games most important methods reside. The play 
				 * method is essentially a springboard for accomplishing most of
				 * the game's functionality.
                 */
                void play();



                /**
                 * Sets the location of the user's saved game when the path 
				 * is provided by the user at the beginning of a game.  
				 * Otherwise, setFilePath is the location and name of the file 
				 * the user wants if the game is being saved while the game is 
                 * in progress.
                 */
                void setFilePath( string str );



                /** Sets the value of wasGameResumed */
                void setWasGameResumed( bool );



                /**
                 * Takes as parameters the minimum value and maximum value
                 * allowed for the integer it seeks.  The function continues
                 * to ask the user for a valid integer until one is supplied.
                 * The valid integer is returned.
                 */
                int getUserInteger( int, int );


};

#endif

