/**AIPlayer is derived from the Player class.  It represents a basic computer
 * that plays its moves in random manner.  A pseudo random number generator 
 * chooses a pile and the number of stones from valid piles and stones.  
 * AIPlayer's move() method overrides Player::move().
 */

#ifndef _AIPLAYER_H_
#define _AIPLAYER_H_

#include <vector>
#include "player.h"


class AIPlayer : public Player
{

    private:

               /**The randomMove() method overrides Player:: move().  This
                * method randomly chooses a pile of stones and a quantity
                * of stones to remove from that pile.
                */
               void randomMove( vector<int>& );



    public:

              /**
                * Default constructor sets the pointer's name and playerType to
                * "AIPlayer", and the variables isYourTurn and
                * isTheWinner to false.
                *
                */
               AIPlayer();


               /* Default destructor */
               ~AIPlayer();

               /**
                * Overrides the move() method of the Player class with its
                * own method called randomMove()
                */
               void move( vector<int>& );


};

#endif
