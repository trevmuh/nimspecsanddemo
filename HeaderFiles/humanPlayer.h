/**humanPlayer is derived from the Player class.  It represents a human player
 * that plays the game through the terminal window.  HumanPlayer overrides and
 * implements the Player::move().
 */


#ifndef _HUMANPLAYER_H_
#define _HUMANPLAYER_H_

#include "player.h"
#include <vector>



class HumanPlayer : public Player
{



    public:
               /**
                * Default constructor sets the pointer's name and playerType to
                * "HumanPlayer", and the variables isYourTurn and
                * isTheWinner to false.
                *
                */
               HumanPlayer();


                /* default destructor */
               ~HumanPlayer();

               /**
                * A virtual function that describes how a player moves.
                * The human player must choose from a valid pile number
                * and a valid number if stones.  The piles of stones are
                * sent into the function as a parameter.
                */
               void move( vector<int>& );

};

#endif


